package com.example.assignment.data.model

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

data class FinchStation(
    val time: Int,
    val stops: List<Stop>,
    val uri: String,
    val name: String)

@Parcelize
data class Stop(
    val uri: String,
    val agency: String,
    val name: String,
    val routes: List<Route>
) : Parcelable

object StationStopsDiffCallback : DiffUtil.ItemCallback<Stop>() {
    override fun areItemsTheSame(oldItem: Stop, newItem: Stop): Boolean =
        oldItem.uri == newItem.uri

    override fun areContentsTheSame(oldItem: Stop, newItem: Stop): Boolean =
        oldItem == newItem
}

@Parcelize
data class Route(
    val uri: String,
    @SerializedName("route_group_id")
    val routeGroupID: Int,
    val name: String,
    @SerializedName("stop_times")
    val stopTimes: List<StopTime>
) : Parcelable

object RouteDiffCallback : DiffUtil.ItemCallback<Route>() {
    override fun areItemsTheSame(oldItem: Route, newItem: Route): Boolean =
        oldItem.routeGroupID == newItem.routeGroupID

    override fun areContentsTheSame(oldItem: Route, newItem: Route): Boolean =
        oldItem == newItem
}

@Parcelize
data class StopTime(
    @SerializedName("service_id")
    val serviceId: Int,
    val shape: String,
    @SerializedName("departure_time")
    val departureTime: String,
    @SerializedName("departure_timestamp")
    val timestamp: Int
) : Parcelable {


    @IgnoredOnParcel
    var from: String = ""
        get() {
            if (field == null) {
                splitShape()
            }

            return field
        }

    @IgnoredOnParcel
    var to: String = ""
        get() {
            if (field == null) {
                splitShape()
            }

            return field
        }

    private fun splitShape() {
        val split = shape.split("To")
        from = split[0]
        to = split[1]
    }
}

object StopTimeDiffCallback : DiffUtil.ItemCallback<StopTime>() {
    override fun areItemsTheSame(oldItem: StopTime, newItem: StopTime): Boolean =
        oldItem.serviceId == newItem.serviceId

    override fun areContentsTheSame(oldItem: StopTime, newItem: StopTime): Boolean =
        oldItem == newItem
}