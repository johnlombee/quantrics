package com.example.assignment.data.api

import com.example.assignment.data.model.FinchStation
import retrofit2.Response
import javax.inject.Inject

class StationApiHelperImpl @Inject constructor(private val stationService: StationService): StationApiHelper {
    override suspend fun getStations(): Response<FinchStation> = stationService.getStations()
}