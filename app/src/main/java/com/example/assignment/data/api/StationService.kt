package com.example.assignment.data.api

import com.example.assignment.data.model.FinchStation
import retrofit2.Response
import retrofit2.http.GET

interface StationService {

    @GET("finch_station.json")
    suspend fun getStations(): Response<FinchStation>
}