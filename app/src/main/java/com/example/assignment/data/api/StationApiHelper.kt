package com.example.assignment.data.api

import com.example.assignment.data.model.FinchStation
import retrofit2.Response

interface StationApiHelper {

    suspend fun getStations(): Response<FinchStation>
}