package com.example.assignment.data.repository

import com.example.assignment.data.api.StationApiHelper
import javax.inject.Inject

class StationRepository @Inject constructor(private val stationApiHelper: StationApiHelper) {

    suspend fun getStations() = stationApiHelper.getStations()
}