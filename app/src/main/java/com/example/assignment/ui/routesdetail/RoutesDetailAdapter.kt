package com.example.assignment.ui.routesdetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.assignment.data.model.Route
import com.example.assignment.data.model.RouteDiffCallback
import com.example.assignment.databinding.ItemRoutesDetailLayoutBinding

/**
 * This class is our Adapter class for displaying list of Routes.
 */
class RoutesDetailAdapter(private val listener: StopDetailAdapterListener) :
    ListAdapter<Route, RoutesDetailViewHolder>(RouteDiffCallback) {

    interface StopDetailAdapterListener {
        /**
         * This method is used to trigger a click event in the list.
         *
         * @param route Route data class
         */
        fun onStopDetailClicked(route: Route)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoutesDetailViewHolder {
        return RoutesDetailViewHolder(
            ItemRoutesDetailLayoutBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            ), listener
        )
    }

    override fun onBindViewHolder(holderTimes: RoutesDetailViewHolder, position: Int) =
        holderTimes.bind(getItem(position))
}