package com.example.assignment.ui.stops

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import com.example.assignment.R
import com.example.assignment.data.model.Stop
import com.example.assignment.databinding.FragmentStopsBinding
import com.example.assignment.ui.BaseFragment
import com.example.assignment.utils.Resource
import com.example.assignment.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

/**
 * This class displays the Stops of a Station.
 * This will be our home Fragment.
 */
@AndroidEntryPoint
class StopsFragment : BaseFragment(), StopsAdapter.StopsAdapterListener {

    private var binding: FragmentStopsBinding by autoCleared()
    private val viewModel: StopsViewModel by viewModels()
    private val stationsAdapter = StopsAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStopsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    /**
     * This method is used to setup our RecyclerView
     */
    private fun setupRecyclerView() {
        // Initialize adapter
        binding.recyclerView.adapter = stationsAdapter
    }

    /**
     * This method is used to setup our Observers
     */
    private fun setupObservers() {
        // Initialize observer for stations
        viewModel.stations.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    // Hide progress bar
                    binding.progressBar.visibility = View.GONE

                    // Populate adapter with list of Stop
                    stationsAdapter.submitList(it.data!!.stops)
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    override fun onStopClicked(stop: Stop) {
        // Navigates to a Fragment, specified by its action along with the required parameter.
        val directions = StopsFragmentDirections.actionStopsFragmentToRoutesDetailFragment(stop)
        navigateTo(directions)
    }
}