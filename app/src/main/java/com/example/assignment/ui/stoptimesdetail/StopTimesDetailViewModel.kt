package com.example.assignment.ui.stoptimesdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.assignment.data.model.Route

class StopTimesDetailViewModel : ViewModel() {

    private val _route : MutableLiveData<Route> = MutableLiveData()
    val route: LiveData<Route> get() = _route

    /**
     * This method is used to set _route
     *
     * @param route Route data class
     */
    fun setRoute(route: Route) { _route.value = route }
}