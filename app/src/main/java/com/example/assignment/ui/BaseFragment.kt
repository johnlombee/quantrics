package com.example.assignment.ui

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.findNavController
import com.example.assignment.R
import com.example.assignment.ui.routesdetail.RoutesDetailFragment
import com.example.assignment.ui.stops.StopsFragment
import com.example.assignment.ui.stoptimesdetail.StopTimesDetailFragment
import com.google.android.material.transition.Hold
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialElevationScale
import com.google.android.material.transition.MaterialFadeThrough

abstract class BaseFragment: Fragment() {

    /**
     * This method is used navigate to a Fragment
     *
     * @param directions NavDirections
     */
    fun navigateTo(directions: NavDirections) {
        findNavController().navigate(directions)
    }

    /**
     * This method is used navigate to a Fragment
     *
     * @param directions NavDirections
     * @param extras Extras
     */
    fun navigateTo(
        directions: NavDirections,
        extras: FragmentNavigator.Extras
    ) {
        findNavController().navigate(directions, extras)
    }
}