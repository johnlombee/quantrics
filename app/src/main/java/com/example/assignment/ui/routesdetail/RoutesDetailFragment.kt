package com.example.assignment.ui.routesdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.navArgs
import com.example.assignment.R
import com.example.assignment.data.model.Route
import com.example.assignment.data.model.Stop
import com.example.assignment.databinding.FragmentRoutesDetailBinding
import com.example.assignment.ui.BaseFragment
import com.example.assignment.utils.autoCleared
import kotlin.LazyThreadSafetyMode.NONE
import dagger.hilt.android.AndroidEntryPoint

/**
 * This class displays the Routes of a Stop from the Station.
 */
@AndroidEntryPoint
class RoutesDetailFragment : BaseFragment(), RoutesDetailAdapter.StopDetailAdapterListener {

    private var binding: FragmentRoutesDetailBinding by autoCleared()
    private val viewModel: RoutesDetailViewModel by viewModels()
    private val stationDetailAdapter = RoutesDetailAdapter(this)

    // Fragment Args
    private val args: RoutesDetailFragmentArgs by navArgs()
    private val stop: Stop by lazy(NONE) { args.stop }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRoutesDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set stop
        viewModel.setStop(stop)

        setupRecyclerView()
        setupObservers()
    }

    /**
     * This method is used to setup our RecyclerView
     */
    private fun setupRecyclerView() {
        // Initialize adapter
        binding.recyclerView.adapter = stationDetailAdapter
    }

    /**
     * This method is used to setup our Observers
     */
    private fun setupObservers() {
        // Initialize observer for a Stop
        viewModel.stop.observe(viewLifecycleOwner, Observer {
            // Populate adapter with list of routes
            stationDetailAdapter.submitList(it.routes)
        })
    }

    override fun onStopDetailClicked(route: Route) {
        // Navigates to a Fragment, specified by its action along with the required parameter.
        val directions = RoutesDetailFragmentDirections.actionRoutesDetailFragmentToStopTimesDetailFragment(route)
        navigateTo(directions)
    }
}