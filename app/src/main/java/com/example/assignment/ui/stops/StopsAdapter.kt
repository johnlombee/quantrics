package com.example.assignment.ui.stops

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.assignment.data.model.Stop
import com.example.assignment.data.model.StationStopsDiffCallback
import com.example.assignment.databinding.ItemStopsLayoutBinding

/**
 * This class is our Adapter class for displaying list of Stop of a station
 */
class StopsAdapter(private val listener: StopsAdapterListener) :
    ListAdapter<Stop, StopsViewHolder>(StationStopsDiffCallback) {

    interface StopsAdapterListener {
        /**
         * This method is used to trigger a click event in the list.
         *
         * @param stop Stop data class
         */
        fun onStopClicked(stop: Stop)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StopsViewHolder {
        return StopsViewHolder(
            ItemStopsLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener
        )
    }

    override fun onBindViewHolder(holder: StopsViewHolder, position: Int) =
        holder.bind(getItem(position))
}