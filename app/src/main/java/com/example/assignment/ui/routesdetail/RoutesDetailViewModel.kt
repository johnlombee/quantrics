package com.example.assignment.ui.routesdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.assignment.data.model.Stop


class RoutesDetailViewModel : ViewModel() {

    private val _stop: MutableLiveData<Stop> = MutableLiveData()
    val stop: LiveData<Stop> get() = _stop

    /**
     * This method is used to set Stop data in _stop
     *
     * @param stop Stop data class
     */
    fun setStop(stop: Stop) { _stop.value = stop }
}