package com.example.assignment.ui.stoptimesdetail

import androidx.recyclerview.widget.RecyclerView
import com.example.assignment.data.model.StopTime
import com.example.assignment.databinding.ItemStopTimesDetailLayoutBinding

/**
 * This class is our ViewHolder for each Stop Times displayed in the list.
 *
 * @param binding Data binding
 */
class StopTimesDetailViewHolder(
    private val binding: ItemStopTimesDetailLayoutBinding
) :
    RecyclerView.ViewHolder(binding.root) {


    fun bind(item: StopTime) {
        binding.stopTime = item
        binding.executePendingBindings()
    }
}