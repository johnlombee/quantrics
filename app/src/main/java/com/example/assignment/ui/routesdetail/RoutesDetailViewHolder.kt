package com.example.assignment.ui.routesdetail

import androidx.recyclerview.widget.RecyclerView
import com.example.assignment.data.model.Route
import com.example.assignment.databinding.ItemRoutesDetailLayoutBinding

/**
 * This class is our ViewHolder for each Route displayed in the list.
 *
 * @param binding Data binding
 * @param listener A reference for our click event
 */
class RoutesDetailViewHolder(
    private val binding: ItemRoutesDetailLayoutBinding,
    listener: RoutesDetailAdapter.StopDetailAdapterListener
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.listener = listener
    }

    fun bind(item: Route) {
        binding.route = item
        binding.executePendingBindings()
    }
}