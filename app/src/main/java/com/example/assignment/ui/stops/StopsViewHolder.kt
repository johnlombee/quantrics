package com.example.assignment.ui.stops

import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment.data.model.Stop
import com.example.assignment.databinding.ItemStopsLayoutBinding

/**
 * This class is our ViewHolder for each Stop displayed in the list.
 *
 * @param binding Data binding
 * @param listener A reference for our click event
 */
class StopsViewHolder(
    private val binding: ItemStopsLayoutBinding,
    listener: StopsAdapter.StopsAdapterListener
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.listener = listener
    }

    fun bind(item: Stop) {
        binding.stop = item
        binding.executePendingBindings()
    }

}