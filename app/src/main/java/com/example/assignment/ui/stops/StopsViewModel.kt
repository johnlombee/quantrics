package com.example.assignment.ui.stops

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assignment.data.model.FinchStation
import com.example.assignment.data.repository.StationRepository
import com.example.assignment.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class StopsViewModel @Inject constructor(private val stationRepository: StationRepository) :
    ViewModel() {

    private val _stations = MutableLiveData<Resource<FinchStation>>()
    val stations: LiveData<Resource<FinchStation>>
        get() = _stations

    init {
        fetchStations()
    }

    /**
     * This method is used to fetch stations
     */
    private fun fetchStations() = viewModelScope.launch {
        _stations.postValue(Resource.loading())
        stationRepository.getStations().let {
            if (it.isSuccessful) {
                _stations.postValue(Resource.success(it.body()))
            } else {
                _stations.postValue(Resource.error(it.errorBody().toString(), null))
            }
        }
    }
}