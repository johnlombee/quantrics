package com.example.assignment.ui.stoptimesdetail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.assignment.data.model.StopTime
import com.example.assignment.data.model.StopTimeDiffCallback
import com.example.assignment.databinding.ItemStopTimesDetailLayoutBinding

/**
 * This class is our Adapter class for displaying list of Stop Times.
 */
class StopTimesDetailAdapter() :
    ListAdapter<StopTime, StopTimesDetailViewHolder>(StopTimeDiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StopTimesDetailViewHolder {
        return StopTimesDetailViewHolder(
            ItemStopTimesDetailLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holderDetail: StopTimesDetailViewHolder, position: Int) {
        holderDetail.bind(getItem(position))
    }
}