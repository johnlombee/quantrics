package com.example.assignment.ui.stoptimesdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.assignment.data.model.Route
import com.example.assignment.databinding.FragmentStopTimesDetailBinding
import com.example.assignment.ui.BaseFragment
import com.example.assignment.utils.autoCleared
import kotlin.LazyThreadSafetyMode.NONE
import dagger.hilt.android.AndroidEntryPoint

/**
 * This class displays the Stop Times or schedule of a Route.
 */
@AndroidEntryPoint
class StopTimesDetailFragment : BaseFragment() {

    private var binding: FragmentStopTimesDetailBinding by autoCleared()
    private val viewModel: StopTimesDetailViewModel by viewModels()
    private val routeAdapter = StopTimesDetailAdapter()

    // Fragment Args
    private val args: StopTimesDetailFragmentArgs by navArgs()
    private val route: Route by lazy(NONE) { args.route }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStopTimesDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Set Route
        viewModel.setRoute(route)

        setupRecyclerView()
        setupObservers()
    }

    /**
     * This method is used to setup our RecyclerView
     */
    private fun setupRecyclerView() {
        // Initialize adapter
        binding.recyclerView.adapter = routeAdapter
    }

    /**
     * This method is used to setup our Observers
     */
    private fun setupObservers() {
        // Initialize observer for a route
        viewModel.route.observe(viewLifecycleOwner, Observer {
            // Populate adapter with list of StopTime
            routeAdapter.submitList(it.stopTimes)
        })
    }
}