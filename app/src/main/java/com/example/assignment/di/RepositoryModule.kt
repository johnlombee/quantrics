package com.example.assignment.di

import com.example.assignment.data.api.StationApiHelper
import com.example.assignment.data.api.StationApiHelperImpl
import com.example.assignment.data.api.StationService
import com.example.assignment.data.repository.StationRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * This class is the Module for our Repositories.
 */
@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun provideStationService(retrofit: Retrofit): StationService =
        retrofit.create(StationService::class.java)

    @Singleton
    @Provides
    fun provideRepository(stationApiHelper: StationApiHelper) = StationRepository(stationApiHelper)

    @Singleton
    @Provides
    fun provideStationApiHelper(stationApiHelper: StationApiHelperImpl): StationApiHelper = stationApiHelper
}